#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <fstream>
#include "Program.h"
#define _USE_MATH_DEFINES
#include <math.h>
#include <string>
using namespace std;

namespace shapes {


	// ���� ���������� ���������� ������ �� �����
	shape* shape::In(ifstream &ifst)
	{
		shape *sp;
		int k;
		ifst >> k;
		switch (k) {
		case 1:
			sp = new rectangle;
			break;
		case 2:
			sp = new circle;
			break;
		case 3:
			sp = new triangle;
			break;
		default:
			return 0;
		}
		sp->InData(ifst);
		return sp;
	}
	
	node::node(shape* data, node* next)
	{
		this->data = data;
		this->next = next;
	}

	// ������� ���������� �� ���������
	// (������������ ������)
	void container::Clear()
	{
		while (head != NULL)
		{
			node* forDelete = head;
			head = head->next;
			delete forDelete;
		}
	}
	// ���� ����������� ���������� �� ���������� ������
	void container::In(ifstream &ifst)
	{
		while (!ifst.eof()) {
			node* last = NULL;
			node* temp = new node(shape::In(ifst), NULL);
			if (ifst.fail() || !ifst)
			{
				head = NULL;
				cout << "Input data is incorrect or the container is empty" << endl;
				break;
			}
			if (head == NULL)
				head = temp;
			else {
				last = head;
				while (last->next != NULL) last = last->next;
				last->next = temp;
			}
		}
	}

	// ����� ����������� ���������� � ��������� �����
	void container::Out(ofstream &ofst)
	{
		Sort();
		node* current = head;
		if (current == NULL)
			return;
		ofst << "Container contains elements:" << endl;
		while (current != NULL)
		{
			current->data->OutData(ofst);	
			current = current->next;
		}
	}

	//���� �����
	void shape::InColor(int colorNumber)
	{
		switch (colorNumber)
		{
		case 1:
			c = shape::color::RED;
			break;
		case 2:
			c = shape::color::ORANGE;
			break;
		case 3:
			c = shape::color::YELLOW;
			break;
		case 4:
			c = shape::color::GREEN;
			break;
		case 5:
			c = shape::color::BLUE;
			break;
		case 6:
			c = shape::color::PURPLE;
			break;
		}
	}

	//����� �����
	void shape::OutColor(ofstream &ofst)
	{
		switch (c)
		{
		case shape::color::RED:
			ofst << "RED" << endl;
			break;
		case shape::color::ORANGE:
			ofst << "ORANGE" << endl;
			break;
		case shape::color::YELLOW:
			ofst << "YELLOW" << endl;
			break;
		case shape::color::GREEN:
			ofst << "GREEN" << endl;
			break;
		case shape::color::BLUE:
			ofst << "BLUE" << endl;
			break;
		case shape::color::PURPLE:
			ofst << "PURPLE" << endl;
			break;
		default:
			ofst << "Color is not defined. Check the input data." << endl;
		}
	}

	void shape::InData(ifstream &ifst) 
	{
		ifst >> density;
	}	void shape::OutData(ofstream &ofst) 
	{
		if (density>0)
			ofst << "density = " << density << endl;
		else
			ofst << "The density must be greater than zero. Check the input data.";

		if (Perimeter() >0)
			ofst << "perimeter = " << Perimeter() << endl;
		else
			ofst << "The perimeter must be greater than zero. Check the input data." << endl;
	}

	// ���� ���������� �������������� �� �����
	void rectangle::InData(ifstream &ifst)
	{
		int c;
		ifst >> x1 >> y1 >> x2 >> y2 >> c;
		InColor(c);
		shape::InData(ifst);
	}
	// ����� ���������� �������������� � �����
	void rectangle::OutData(ofstream &ofst)
	{
		if (x1 != x2 && y1 != y2)
		{
			ofst << "It is Rectangle: x1 = " << x1
				<< ", y1 = " << y1
				<< ", x2 = " << x2
				<< ", y2 = " << y2 << ", color: ";
			OutColor(ofst);
			shape::OutData(ofst);
			ofst << endl;
		}
		else
			ofst << "Attention! Check the input data. The coordinates of the points must be different." << endl;
	}

	float rectangle::Perimeter()
	{
		int p;
		if(x1 != x2 && y1 != y2)
			p = (abs(x2 - x1)+abs(y2 - y1))*2;
		else 
			p = 0;
		return p;
	}


	// ���� ���������� ����� �� ������
	void circle::InData(ifstream &ifst)
	{
		int c;
		ifst >> a >> b >> r >> c;
		InColor(c);
		shape::InData(ifst);
	}
	// ����� ���������� ����� � ����
	void circle::OutData(ofstream &ofst)
	{
		if (r > 0)
		{
			ofst << "It is Circle: a = "
				<< a << ", b = " << b
				<< ", r = " << r << ", color: ";
			OutColor(ofst);
			shape::OutData(ofst);
			ofst << endl;
		}
		else
			ofst << "Attention! Check the input data. The radius of the circle must be greater than zero." << endl;
	}

	float circle::Perimeter()
	{
		return 2 * M_PI*r;
	}

	// ���� ���������� ������������ �� ������
	void triangle::InData(ifstream &ifst)
	{
		int c;
		ifst >> a1 >> b1 >> a2 >> b2 >> a3 >> b3 >> c;
		InColor(c);
		shape::InData(ifst);
	}
	// ����� ���������� ������������ � ����
	void triangle::OutData(ofstream &ofst)
	{
		if ((a1 != a2 || b1 != b2) && (a1 != a3 || b1 != b3) && (a2 != a3 || b2 != b3))
		{
			ofst << "It is Triangle: x1 = " << a1
				<< ", y1 = " << b1
				<< ", x2 = " << a2
				<< ", y2 = " << b2
				<< ", x3 = " << a3
				<< ", y3 = " << b3 << ", color: ";
			OutColor(ofst);
			shape::OutData(ofst);
			ofst << endl;
		}
		else
			ofst << "Attention! Check the input data. The coordinates of the points must be different." << endl;
	}

	float triangle::Perimeter()
	{
		float a;
		float b;
		float c;
		a = sqrt((a2 - a1)*(a2 - a1) + (b2 - b1)*(b2 - b1));
		b = sqrt((a3 - a1)*(a3 - a1) + (b3 - b1)*(b3 - b1));
		c = sqrt((a3 - a2)*(a3 - a2) + (b3 - b2)*(b3 - b2));
		if (a == 0 || b == 0 || c == 0)
			return 0;
		else
			return a + b + c;
	}
	
	// ��������� ���� ��������
	bool shape::Compare(shape &other) {
		return Perimeter() > other.Perimeter();
	}

	void Swap(node* one, node* two)
	{
		shape* temp = one->data;
		one->data = two->data;
		two->data = temp;
	}
	// ���������� ����������� ����������
	void container::Sort()
	{
		bool flag = true;
		while (flag)
		{
			flag = false;
			node* current = head;
			if (current == NULL)
				return;
			while (current->next != NULL)
			{
				if ((current->data->Compare(*current->next->data)) == true)
				{
					Swap(current, current->next);
					flag = true;
				}
				current = current->next;
			}
		}
	}
	// ����� ������ ������ ��� ��������������
	void shape::OutRect(ofstream &ofst) 
	{
		ofst << ""; // ������ ������
	}
	void rectangle::OutRect(ofstream &ofst) 
	{
		OutData(ofst);
	}

	void container::OutRect(ofstream &ofst) {
		ofst << "Only rectangles." << endl;
		node* current = head;
		if (current == NULL)
			return;
		while (current != NULL)
		{
			current->data->OutRect(ofst);
			current = current->next;
		}
	}

	//���������� ������ � ���������
	void container::InCont(shape * sp)
	{
			node* last = NULL;
			node* temp = new node(sp, NULL);
			if (head == NULL)
				head = temp;
			else {
				last = head;
				while (last->next != NULL) last = last->next;
				last->next = temp;
		}
	}

	shape* container::OutCont(int i)
	{
		node *temp = head; 
		int j = 0;
		while (temp != NULL) 
		{
			if (j == i)
				return (temp->data);
			j++;
			temp = temp->next;
		}
	}

} // end simple_shapes namespace