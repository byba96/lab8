
#ifndef __ProgramOOP__
#define __ProgramOOP__

#include <fstream>
using namespace std;

namespace shapes {

	// �����, ����������� ��� ��������� ������. 
	class shape
	{	
	protected:
		float density;
		void InColor(int colorNumber);
		void OutColor(ofstream &ofst);
	public:
		enum color { RED, ORANGE, YELLOW, GREEN, BLUE, DARKBLUE, PURPLE };
		color c;
		shape() {};
		shape(color color1, float dens):c(color1),density(dens) {};
		static shape* In(ifstream &ifst);
		virtual void InData(ifstream &ifst); 
		virtual void OutData(ofstream &ofst);
		virtual float Perimeter() = 0;
		bool Compare(shape &other);
		// ����� ������ ���������������
		virtual void OutRect(ofstream &ofst);
	};
	// �������������
	class rectangle : public shape
	{
	private:
		int x1, y1, x2, y2; // ������������� ���������� ������ �������� � ������� ������� �����

	public:
		void InData(ifstream &ifst); 
		void OutData(ofstream &ofst); 
		float Perimeter();
		void OutRect(ofstream &ofst);
		rectangle() {}; 
		rectangle(int x11, int y11, int x22, int y22, color color1, float dens):
			x1(x11), y1(y11), x2(x22), y2(y22), shape(color1, dens) {};
	};

	//����
	class circle : public shape
	{
	private:
		int a, b, r; // ������������� ���������� ������ ����������, ������
	public:
		void InData(ifstream &ifst); 
		void OutData(ofstream &ofst); 
		float Perimeter();
		circle() {}; 
		circle(int a1, int b1, int r1, color color1, float dens):
			a(a1), b(b1), r(r1), shape(color1, dens) {};
	};

	//�����������
	class triangle : public shape
	{
	private:
		int a1, b1, a2, b2, a3, b3; // ��� �����, �������� ������������� ���������� ������
	public:
		void InData(ifstream &ifst);
		void OutData(ofstream &ofst);
		float Perimeter();
		triangle() {};
		triangle(int a11, int b11, int a22, int b22, int a33, int b33, color color1, float dens) :
			a1(a11), b1(b11), a2(a22), b2(b22), a3(a33), b3(b33), shape(color1, dens) {};
	};


	//��������� �� ������ ����������� ��������� ������
	class node
	{
	public:
		shape* data;
		node* next;
		node(shape* data, node* next);
	};

	class container
	{
	public:
		node* head;
		container() { head = NULL;  }
		void In(ifstream &ifst);
		void Out(ofstream &ofst);
		void Perimeter(ofstream &ofst);
		void Sort();
		void OutRect(ofstream &ofst);
		void InCont(shape * sp);
		shape* OutCont(int i);
		void Clear();
		~container() { Clear(); }
	};

}// end simple_shapes namespace
#endif