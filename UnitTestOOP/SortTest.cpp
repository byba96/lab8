#include "stdafx.h"
#include "CppUnitTest.h"
#include "Program.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace shapes;

namespace SortTest
{
	TEST_CLASS(SortTest)
	{
	public:

		TEST_METHOD(Sort)
		{
			container *c = new container();
			shape *r1 = new rectangle(0, 0, 2, 2, shape::color::RED, 1);
			shape *r2 = new rectangle(0, 0, 3, 2, shape::color::RED, 1);
			shape *c1 = new circle(0, 0, 1, shape::color::RED, 1);
			shape *c2 = new circle(1, 2, 2, shape::color::RED, 1);
			shape *tr1 = new triangle(0, 0, 0, 3, 4, 0, shape::color::RED, 1);

			c->InCont(r1);
			c->InCont(r2);
			c->InCont(c1);
			c->InCont(c2);
			c->InCont(tr1);

			container *SortC = new container();
			SortC->InCont(c1);
			SortC->InCont(r1);
			SortC->InCont(r2);
			SortC->InCont(tr1);
			SortC->InCont(c2);

			c->Sort();

				for (int i = 0; i < 5; i++)
				{
					shape* s = c->OutCont(i);
					shape* expected = SortC->OutCont(i);
					Assert::AreEqual(expected->StringForAssert(), s->StringForAssert());
				}
		}

	};
}
