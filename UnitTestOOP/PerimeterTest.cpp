#include "stdafx.h"
#include "CppUnitTest.h"
#include "Program.h"
#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <math.h>

using namespace std;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace shapes;

namespace PerimeterTest
{
	TEST_CLASS(PerimeterTest)
	{
	public:

		TEST_METHOD(PerimeterCircle)
		{
			float expected = 2* M_PI;
			shape *sp = new circle(1, 1, 1, shape::color::RED, 1);
			float p = sp->Perimeter();
			Assert::AreEqual(expected, p);
		}

		TEST_METHOD(PerimeterRectangle)
		{
			float expected = 8.0;
			shape *sp = new rectangle(0, 0, 2, 2, shape::color::RED, 1);
			float p = sp->Perimeter();
			Assert::AreEqual(expected, p);
		}

		TEST_METHOD(PerimeterTriangle)
		{
			float expected = 12.0;
			shape *sp = new triangle(0, 0, 0, 3, 4, 0, shape::color::RED, 1);
			float p = sp->Perimeter();
			Assert::AreEqual(expected, p);
		}

	};
}
